package ge.lanmaster.json2sqlite_demo;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import ge.lanmaster.json2sqlite.DBOperationsManager;
import ge.lanmaster.json2sqlite.pojp.Reference;
import ge.lanmaster.json2sqlite.util.DBOpenHelper;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    DBOpenHelper mDBOpenHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.button1).setOnClickListener(this);
        findViewById(R.id.button2).setOnClickListener(this);

        mDBOpenHelper = new DBOpenHelper(this, "ebay_db");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button1:
                Toast.makeText(this, "B1", Toast.LENGTH_LONG).show();

                StringBuilder out = new StringBuilder();

                try {
                    InputStream is = getAssets().open("response.json");
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is));

                    String line;
                    while ((line = reader.readLine()) != null) {
                        out.append(line);
                    }
                } catch (IOException e) {
                    Log.e("TAG", "MSG", e);
                }

                try {
                    JSONObject json = new JSONObject(out.toString());
                    JSONObject object = json.getJSONObject("getTopSellingProductsResponse");
                    JSONObject object1 = object.getJSONObject("productRecommendations");
                    JSONArray array = object1.getJSONArray("product");

                    SQLiteDatabase db = mDBOpenHelper.getWritableDatabase();
                    DBOperationsManager manager = new DBOperationsManager(db);
                    Reference reference = manager.persistRootObject(json);

                    manager.deleteRootObject(reference);

//                    JSONObject recoveredObject = new JSONObject();
//                    JSONObjectHelper recoveredObjectHelper = new JSONObjectHelper(recoveredObject);
//                    manager.retrieveRootObject(reference, recoveredObjectHelper);
//
//                    JSONObject temp = recoveredObject.getJSONObject("_data");
//                    temp = temp.getJSONObject("getTopSellingProductsResponse");
//                    temp = temp.getJSONObject("productRecommendations");
//                    JSONArray tempa = temp.getJSONArray("product");
//                    temp = tempa.getJSONObject(0);
//
//                    manager.deleteObject(temp.getLong(Constants.ID));
//
//                    recoveredObject = new JSONObject();
//                    recoveredObjectHelper = new JSONObjectHelper(recoveredObject);
//                    manager.retrieveRootObject(reference, recoveredObjectHelper);
//                    recoveredObject.toString();
                } catch (JSONException e) {
                    Log.e("TAG", "MSG", e);
                }

                break;
            case R.id.button2:
                Toast.makeText(this, "B2", Toast.LENGTH_LONG).show();
                break;
        }
    }
}
